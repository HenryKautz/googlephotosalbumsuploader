import os
import pickle
import sys
import requests
import argparse
import re   
#from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from googleapiclient.discovery import build
#from googleapiclient.errors import HttpError

DryRun = False

# OAuth2 authentication
def authenticate():
    creds = None
    SCOPES = ['https://www.googleapis.com/auth/photoslibrary']
    CLIENT_SECRET_FILE = 'client_secret.json'
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRET_FILE, SCOPES)
            creds = flow.run_local_server(port=0)
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)
    return creds

def create_album(service, album_name):
    if DryRun:
        response = { 'id':0 }
        return response
    album_metadata = { 'album': {'title': album_name} }
    try:
        response = service.albums().create(body=album_metadata).execute()
        return response
    except Exception as error:
        print(f"An error occurred trying to create album {album_name}: {error}")
        raise RuntimeError('Create album failed') from error

mediaMatcher = re.compile(r"[^.].*\.(" +
    r"BMP|GIF|HEIC|ICO|JPG|JPEG|PNG|TIFF|WEBP|RAW" +
    r"3GP|3G2|ASF|AVI|DIVX|M2T|M2TS|M4V|MKV|MMV|MOD|MOV|MP4|MPG|MTS|TOD|WMV" +
    r")", re.IGNORECASE)

def is_media(filename):
    # Return true if filename has a photo or video and is not hidden
    return mediaMatcher.fullmatch(filename)

def upload_photos(creds, service, folder_paths):
    failures = []
    for folder_path in folder_paths:
        if not os.path.isdir(folder_path):
            print(f"Bad argument {folder_path}, it is not a folder")
            continue   
        for root, dirs, files in os.walk(folder_path):
            # sort dirs in place, so at next level they are visited alphabetically
            dirs.sort()
            # determine if root has media files and so will become an album
            mediaFiles = [ f for f in files if is_media(f) ]
            if len(mediaFiles)>0:
                # create an album corresponding to root
                mediaFiles.sort()
                album_name = os.path.basename(root)
                album_path = root
                try:
                    album = create_album(service, album_name)
                    album_id = album['id']
                except Exception:
                    print(f"Continuing on to next album")
                    failures.append(['Album creation failure:', album_name, 'from', album_path])
                    continue
                print(f"Created album: {album_name}")
                # upload the media into the album
                for file in mediaFiles:
                    file_path = os.path.join(album_path, file)
                    try:
                        upload_photo(creds, service, file_path, file, album_id)
                    except Exception:
                        failures.append(['Media upload failure:', file, 'in', album_name, 'from', file_path])
                        print("Continuing to next file")
    print(f"Number of failures: {len(failures)}")
    for failure in failures:
        print(' '.join(failure))

def upload_photo(creds, service, file_path, file_name, album_id):
    if DryRun:
        print(f"Uploaded: {file_name}")
        return
    try:
        headers = {
            'Authorization': 'Bearer ' + creds.token,
            'Content-type': 'application/octet-stream',
            'X-Goog-Upload-Protocol': 'raw',
            'X-Goog-Upload-File-Name': file_name }
        img = open(file_path, 'rb').read()
        uploadResponse = requests.post('https://photoslibrary.googleapis.com/v1/uploads', 
                                       data=img, headers=headers)
        uploadToken = uploadResponse.content.decode('utf-8')
        _ = service.mediaItems().batchCreate(
            body={"newMediaItems": [{
                    "description": file_name,
                    "simpleMediaItem": {
                        "fileName": file_name,
                        "uploadToken": uploadToken}}], 
                "albumId": album_id}).execute()
        print(f"Uploaded: {file_name}")
    except Exception as error:
        print(f"Could not upload file {file_name} due to error {error}")
        raise RuntimeError('Media upload failed') from error



def main():
    parser = argparse.ArgumentParser(description='Upload photo albums to Google Photos.')
    parser.add_argument('-dryrun', action='store_true')
    parser.add_argument('folderpaths', metavar='dirs', nargs='+', 
        type=os.path.abspath,
        help='One or more folders containing photos and/or subfolders')
    args = parser.parse_args()
    global DryRun
    DryRun = args.dryrun
    if DryRun:
        print(f"Dry run protocol, folderpaths = {args.folderpaths} ")
    creds = authenticate()
    service = build('photoslibrary', 'v1', credentials=creds, static_discovery=False)
    upload_photos(creds, service, args.folderpaths )

if __name__ == '__main__':
    main()