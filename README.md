ghotos\_upload\_albums.py

Required python libraries:

pip install google-auth google-auth-oauthlib google-auth-httplib2 google-api-python-client

Before using application, follow these steps to create an OAUTH token:

1. Go to the Google API Console.
2. Create a new project or select an existing one.
3. Click on "Enable APIs and Services" and search for "Google Photos Library API". Enable it.
4. Click "Create credentials" and choose "OAuth client ID".
5. Select "Desktop app" as the application type, enter a name, and click "Create".
6. Download the client_secret JSON file and save it to the project folder. Rename it as client_secret.json.
7. Go to the Oauth Consent Screen and add yourself as a Test user.

Use: where the PATH argument (possibly repeated) is the path to a folder containing media or subfolders, 
executing

    python gphotos_upload_albums.py PATH {PATH ...}

will upload the media to newly created albums in Google Photos.  The name of each album is
the name of the folder that contained the media.  Google Photos does not support nested albums, so if the folders are hierarchically organized the created albums will ignore the hierarchical structure. No album is created for folders that are empty or that only contain subfolders.  

Note: The program always creates new albums, so if an album already exists with the same name, then Google Photos will end up containing two albums with the same name. This behavior is necessary because the Google Photos API does not allow an application to add media to an album it did not create in the same session.




